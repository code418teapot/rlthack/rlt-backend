FROM bellsoft/liberica-openjdk-alpine:17
EXPOSE 8080
ADD /build/libs/game-app*.jar /usr/local/game-app/game-app.jar
ENTRYPOINT ["java", "-Dapp.home=/usr/src/game-app/home", "-jar", "/usr/local/game-app/game-app.jar"]
