package ru.team418.rlt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class GameAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameAppApplication.class, args);
	}

	/*
    Бин шифровщика/дешифровщика паролей
     */
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
