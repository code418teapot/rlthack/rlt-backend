package ru.team418.rlt.telegram.enums;

public enum CommandType {

    MESSAGE,
    COMMAND,
    BUTTON

}
