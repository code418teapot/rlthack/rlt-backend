package ru.team418.rlt.telegram.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.team418.rlt.telegram.services.ArticleService;
import ru.team418.rlt.telegram.views.ArticleView;

@RestController("/telegram/articles")
public class ArticleController {

    private final ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping
    public void add(@RequestBody ArticleView articleView) {
        articleService.save(articleView);
    }

}
