package ru.team418.rlt.telegram.services;

import ru.team418.rlt.telegram.dto.Notice;

public interface NoticeBot {

    void sendNotice(Notice notice);

    void sendNotice(String description, Long telegramId);
}
