package ru.team418.rlt.telegram.services;

import org.springframework.stereotype.Service;
import ru.team418.rlt.gameapp.service.infrastructure.UserService;
import ru.team418.rlt.telegram.enteties.TelegramIntegrationCode;
import ru.team418.rlt.telegram.repositories.TelegramIntegrationCodeRepository;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class TelegramIntegrationService {

    private final TelegramIntegrationCodeRepository repository;
    private final UserService userService;

    public TelegramIntegrationService(TelegramIntegrationCodeRepository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    public String generateCode(Long userId) {
        var code = UUID.randomUUID();
        repository.save(new TelegramIntegrationCode(userId, code.toString()));
        return code.toString();
    }

    public boolean integrate(String code, Long chatId) {
        var integration = repository.findByCode(code);
        if (integration == null) {
            throw new EntityNotFoundException();
        }
        var user = userService.findUserById(integration.getUserId());
        if (user == null) {
            throw new EntityNotFoundException();
        }
        if (user.getTelegramId() != null) {
            return false;
        }
        user.setTelegramId(chatId);
        userService.save(user);
        return true;
    }

}
