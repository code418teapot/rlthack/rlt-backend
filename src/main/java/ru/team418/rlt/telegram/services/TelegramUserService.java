package ru.team418.rlt.telegram.services;

import org.springframework.stereotype.Service;
import ru.team418.rlt.telegram.enteties.TelegramUser;
import ru.team418.rlt.telegram.repositories.TelegramUserRepository;

@Service
public class TelegramUserService {

    private final TelegramUserRepository repository;

    public TelegramUserService(TelegramUserRepository repository) {
        this.repository = repository;
    }

    public TelegramUser create(Long id) {
        if (!repository.existsById(id)) {
            return repository.save(new TelegramUser(id));
        } else {
            return null;
        }
    }

    public TelegramUser find(Long id) {
        return repository.findById(id).orElse(null);
    }

    public void save(TelegramUser tUser) {
        repository.save(tUser);
    }

}
