package ru.team418.rlt.telegram.views;

public record PhotoView(
        Long id,
        String url) {
}
