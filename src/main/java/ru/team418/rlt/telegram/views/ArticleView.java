package ru.team418.rlt.telegram.views;

import java.util.Collection;

public record ArticleView(
        Long id,
        String command,
        String text,
        Collection<PhotoView> photos,
        Collection<LinkButtonView> buttons) {
}
