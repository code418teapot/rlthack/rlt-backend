package ru.team418.rlt.telegram.views;

public record LinkButtonView(
        Long id,
        String codeTo,
        String text) {
}
