package ru.team418.rlt.telegram.enteties;

import ru.team418.rlt.telegram.views.LinkButtonView;

import javax.persistence.*;

@Entity
public class LinkButton {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String codeTo;
    private String text;


    public LinkButton(LinkButtonView linkButtonView) {
        this.id = linkButtonView.id();
        this.codeTo = linkButtonView.codeTo();
        this.text = linkButtonView.text();
    }

    protected LinkButton() {
    }

    public Long getId() {
        return id;
    }

    public String getCodeTo() {
        return codeTo;
    }

    public String getText() {
        return text;
    }

}
