package ru.team418.rlt.telegram.enteties;

import ru.team418.rlt.telegram.enums.BotState;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class TelegramUser {

    @Id
    private Long id;

    @Enumerated(EnumType.STRING)
    private BotState state;

    protected TelegramUser() {
    }

    public TelegramUser(Long id) {
        this.id = id;
        this.state = BotState.NOTHING;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BotState getState() {
        return state;
    }

    public void setState(BotState state) {
        this.state = state;
    }
}
