package ru.team418.rlt.telegram.enteties;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class TelegramIntegrationCode {

    @Id
    private Long userId;
    private String code;

    public TelegramIntegrationCode(Long userId, String code) {
        this.userId = userId;
        this.code = code;
    }

    protected TelegramIntegrationCode() {
    }

    public Long getUserId() {
        return userId;
    }

    public String getCode() {
        return code;
    }
}
