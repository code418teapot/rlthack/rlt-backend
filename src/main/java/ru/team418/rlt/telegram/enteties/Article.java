package ru.team418.rlt.telegram.enteties;

import ru.team418.rlt.telegram.views.ArticleView;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String command;
    @Lob
    private String text;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "article_id")
    private Set<Photo> photos;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "article_id")
    private Set<LinkButton> buttons;

    public Article(ArticleView articleView) {
        id = articleView.id();
        command = articleView.command();
        text = articleView.text();
        photos = articleView.photos().stream().map(Photo::new).collect(Collectors.toSet());
        buttons = articleView.buttons().stream().map(LinkButton::new).collect(Collectors.toSet());
    }

    protected Article() {
    }

    public Long getId() {
        return id;
    }

    public String getCommand() {
        return command;
    }

    public String getText() {
        return text;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public Set<LinkButton> getButtons() {
        return buttons;
    }
}
