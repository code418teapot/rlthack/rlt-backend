package ru.team418.rlt.telegram.enteties;

import ru.team418.rlt.telegram.views.PhotoView;

import javax.persistence.*;

@Entity
public class Photo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String url;

    public Photo(PhotoView photoView) {
        id = photoView.id();
        url = photoView.url();
    }

    protected Photo() {

    }

    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
