package ru.team418.rlt.telegram.dto;

import ru.team418.rlt.telegram.enums.CommandType;

public record CommandDto(String command, Long userId, CommandType commandType) {
}
