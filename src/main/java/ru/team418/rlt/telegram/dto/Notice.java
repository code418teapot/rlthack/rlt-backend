package ru.team418.rlt.telegram.dto;

public record Notice(String buttonName, String description, String url, String imageUrl, Long id) {
}
