package ru.team418.rlt.telegram.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team418.rlt.telegram.enteties.Article;

import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    Optional<Article> findByCommand(String command);
}
