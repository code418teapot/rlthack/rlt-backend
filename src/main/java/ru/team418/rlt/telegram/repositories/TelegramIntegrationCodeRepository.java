package ru.team418.rlt.telegram.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team418.rlt.telegram.enteties.TelegramIntegrationCode;

import java.util.UUID;

public interface TelegramIntegrationCodeRepository extends JpaRepository<TelegramIntegrationCode, Long> {

    TelegramIntegrationCode findByCode(String code);

}
