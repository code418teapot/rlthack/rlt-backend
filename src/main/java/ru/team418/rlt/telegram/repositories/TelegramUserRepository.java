package ru.team418.rlt.telegram.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.team418.rlt.telegram.enteties.TelegramUser;

public interface TelegramUserRepository extends JpaRepository<TelegramUser, Long> {
}
