package ru.team418.rlt.telegram;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import ru.team418.rlt.gameapp.service.infrastructure.UserService;
import ru.team418.rlt.telegram.dto.CommandDto;
import ru.team418.rlt.telegram.dto.Notice;
import ru.team418.rlt.telegram.enteties.Article;
import ru.team418.rlt.telegram.enteties.LinkButton;
import ru.team418.rlt.telegram.enteties.Photo;
import ru.team418.rlt.telegram.enteties.TelegramUser;
import ru.team418.rlt.telegram.enums.BotState;
import ru.team418.rlt.telegram.enums.CommandType;
import ru.team418.rlt.telegram.services.ArticleService;
import ru.team418.rlt.telegram.services.NoticeBot;
import ru.team418.rlt.telegram.services.TelegramIntegrationService;
import ru.team418.rlt.telegram.services.TelegramUserService;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static ru.team418.rlt.telegram.enums.BotState.INPUT_INTEGRATION_TOKEN;
import static ru.team418.rlt.telegram.enums.BotState.NOTHING;

@Component
public class RltBot extends TelegramLongPollingBot implements NoticeBot {

    private static final Random RANDOM = new Random();

    @Value("${rlt-bot.api-path}")
    private String apiUri;
    @Value("${rlt-bot.bot-name}")
    private String botName;
    @Value("${rlt-bot.first-article}")
    private String firstArticle;

    private final TelegramUserService telegramUserService;
    private final TelegramIntegrationService telegramIntegrationService;
    private final ArticleService articleService;
    private final UserService userService;

    public RltBot(
            @Value("${rlt-bot.token}") String token,
            TelegramUserService telegramUserService,
            TelegramIntegrationService telegramIntegrationService,
            ArticleService articleService, UserService userService) {
        super(token);
        this.telegramUserService = telegramUserService;
        this.telegramIntegrationService = telegramIntegrationService;
        this.articleService = articleService;
        this.userService = userService;
    }

    @Override
    public void onUpdateReceived(Update update) {
        CommandDto commandDto = buildCommand(update);
        try {
            TelegramUser tUser = userExistenceAdapter(commandDto);
            if (tUser.getState() == NOTHING) {
                if (commandDto.command().equals("/start")) {
                    init(commandDto.userId());
                }
                if (commandDto.command().startsWith("/article")) {
                    processArticles(commandDto);
                } else if (commandDto.command().equals("/integration")) {
                    tUser.setState(BotState.INPUT_INTEGRATION_TOKEN);
                    telegramUserService.save(tUser);
                    var sm = SendMessage.builder()
                            .chatId(commandDto.userId())
                            .text("Введите код").build();
                    execute(sm);
                }
            } else if (tUser.getState() == INPUT_INTEGRATION_TOKEN) {
                integrate(commandDto, tUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
            var sm = SendMessage.builder()
                    .text("Что-то пошло не так")
                    .chatId(commandDto.userId())
                    .build();
            try {
                execute(sm);
            } catch (TelegramApiException telegramApiException) {
                telegramApiException.printStackTrace();
            }
        }
    }

    private void integrate(CommandDto commandDto, TelegramUser tUser) throws TelegramApiException {
        String response = null;
        try {
            if (telegramIntegrationService.integrate(commandDto.command(), commandDto.userId())) {
                response = "Интеграция прошла успешно";
            } else {
                response = "Телеграмм уже был интегрирован";
            }
        } catch (EntityNotFoundException e) {
            response = "Данные некорректны";
        } finally {
            if (response == null) {
                response = "что-то пошло не так";
            }
            var sm = SendMessage.builder()
                    .chatId(commandDto.userId())
                    .text(response)
                    .build();
            execute(sm);
            tUser.setState(BotState.NOTHING);
            telegramUserService.save(tUser);
        }
    }

    private void processArticles(CommandDto commandDto) throws TelegramApiException {
        var article = articleService.getByCommand(commandDto.command());
        sendImages(commandDto.userId(), article.getPhotos());
        sendMessage(commandDto.userId(), article);
    }

    private String getRegisterUri(Long chatId) {
        return String.format("%s/authentication/create-profile?telegramId=%s", apiUri, chatId);
    }

    private void sendMessage(Long chatId, Article article) throws TelegramApiException {
        var kbBuilder = InlineKeyboardMarkup.builder();
        article.getButtons().forEach(b -> kbBuilder.keyboardRow(List.of(this.convert(b))));
        var kb = kbBuilder.build();

        var sm = SendMessage.builder()
                .chatId(chatId)
                .text(article.getText())
                .replyMarkup(kb)
                .build();
        execute(sm);
    }

    private void sendImages(Long chatId, Collection<Photo> photos) throws TelegramApiException {
        if (photos == null || photos.isEmpty()) {
            return;
        } else if (photos.size() == 1) {
            var sp = SendPhoto.builder()
                    .photo(new InputFile(photos.stream().findFirst().get().getUrl()))
                    .chatId(chatId)
                    .build();
            execute(sp);
        } else {
            var smg = SendMediaGroup.builder()
                    .medias(photos.stream().map(this::convert).collect(Collectors.toList()))
                    .chatId(chatId)
                    .build();
            execute(smg);
        }
    }

    private InputMediaPhoto convert(Photo photo) {
        return InputMediaPhoto.builder().media(photo.getUrl()).build();
    }

    private InlineKeyboardButton convert(LinkButton linkButton) {
        return InlineKeyboardButton.builder().text(linkButton.getText()).callbackData(linkButton.getCodeTo()).build();
    }

    private TelegramUser userExistenceAdapter(CommandDto commandDto) {
        var tUser = telegramUserService.find(commandDto.userId());
        if (tUser == null) {
            tUser = telegramUserService.create(commandDto.userId());
        } else {
            var user = userService.findByTelegramId(commandDto.userId());
            if (user == null || user.getTelegramId() == null) {
                registrationPurpose(commandDto.userId());
            }
        }
        return tUser;
    }

    private void registrationPurpose(Long chatId) {
        if (RANDOM.nextInt(1, 20) == 10) {
            var button = InlineKeyboardButton.builder()
                    .text("Привязать")
                    .url(getRegisterUri(chatId))
                    .build();
            var kb = InlineKeyboardMarkup.builder()
                    .keyboardRow(List.of(button))
                    .build();
            var sm = SendMessage.builder()
                    .text("Привязать телеграм к сайту Росэлторг-бизнес")
                    .replyMarkup(kb)
                    .chatId(chatId)
                    .build();
            try {
                execute(sm);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    private CommandDto buildCommand(Update update) {
        if (update.getCallbackQuery() != null) {
            return new CommandDto(
                    update.getCallbackQuery().getData(),
                    update.getCallbackQuery().getFrom().getId(),
                    CommandType.BUTTON);
        } else if (update.getMessage() != null) {
            Message message = update.getMessage();
            if (message.isCommand()) {
                return new CommandDto(
                        message.getText(),
                        message.getFrom().getId(),
                        CommandType.MESSAGE);
            } else {
                return new CommandDto(
                        message.getText(),
                        message.getFrom().getId(),
                        CommandType.COMMAND);
            }
        } else {
            return null;
        }
    }

    public void init(Long chatId) {
        super.onRegister();
        var btToSite = InlineKeyboardButton.builder()
                .text("Перейти на сайт")
                .url("https://rlthack.team418.ru/")
                .build();
        var btToArticles = InlineKeyboardButton.builder()
                .text("Почитать интересные статьи")
                .callbackData("/article_1")
                .build();
        var btToTree = InlineKeyboardButton.builder()
                .text("Акция \"Посади дерево\"")
                .url("http://greenrsl.tilda.ws/")
                .build();
        var br = InlineKeyboardMarkup.builder()
                .keyboardRow(List.of(btToSite))
                .keyboardRow(List.of(btToArticles))
                .keyboardRow(List.of(btToTree))
                .build();
        var sm = SendMessage.builder()
                .text("Привет, я электронный помошник \"Росэлторг-бизнес\".")
                .replyMarkup(br)
                .chatId(chatId)
                .build();
        try {
            execute(sm);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @PostConstruct
    public void doSomethingAfterStartup() {
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(this);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendNotice(Notice notice) {
        InlineKeyboardButton kb = InlineKeyboardButton.builder()
                .text(notice.buttonName())
                .url(notice.url())
                .build();
        InlineKeyboardMarkup km = InlineKeyboardMarkup.builder()
                .keyboardRow(List.of(kb))
                .build();
        SendPhoto sp = SendPhoto.builder()
                .chatId(notice.id())
                .photo(new InputFile().setMedia(notice.imageUrl()))
                .caption(notice.description())
                .replyMarkup(km)
                .build();
        try {
            execute(sp);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendNotice(String description, Long telegramId) {
        SendMessage sendMessage = SendMessage.builder()
                .text(description)
                .chatId(telegramId)
                .build();
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
