package ru.team418.rlt.gameapp.service;

import static org.springframework.util.StringUtils.isEmpty;

public final class Utils {

    private Utils() {
    }

    /**
     * Получить полное ФИО
     *
     * @param surName    фамилия
     * @param firstName  имя
     * @param middleName отчество
     *
     * @return полное ФИО
     */
    public static String buildFio(String surName, String firstName, String middleName) {
        StringBuilder result = new StringBuilder();
        result.append(surName);
        if (!isEmpty(firstName)) {
            result.append(' ');
            result.append(firstName);
        }
        if (!isEmpty(middleName)) {
            result.append(' ');
            result.append(middleName);
        }
        return result.toString();
    }

    /**
     * Фамилия и инициалы
     *
     * @param surName    фамилия
     * @param firstName  имя
     * @param middleName отчество
     *
     * @return Фамилия и инициалы
     */
    public static String buildInitials(String surName, String firstName, String middleName) {
        if (isEmpty(surName)) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        result.append(surName);
        if (!isEmpty(firstName)) {
            result.append(' ');
            result.append(firstName.charAt(0));
            result.append(". ");
        }
        if (!isEmpty(middleName)) {
            result.append(' ');
            result.append(middleName.charAt(0));
            result.append('.');
        }
        return result.toString();
    }
}
