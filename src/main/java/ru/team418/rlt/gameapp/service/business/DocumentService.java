package ru.team418.rlt.gameapp.service.business;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.team418.rlt.gameapp.dto.DocumentDto;
import ru.team418.rlt.gameapp.entity.Company;
import ru.team418.rlt.gameapp.entity.Document;
import ru.team418.rlt.gameapp.exceptions.NotAvailableOperationException;
import ru.team418.rlt.gameapp.repository.DocumentRepository;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class DocumentService {

    private DocumentRepository documentRepository;
    private CompanyService companyService;
    private NoticeService noticeService;

    public List<Document> findByCompany(Long companyId) {
        return documentRepository.findByCompanyId(companyId);
    }

    public Document sign(Long companyId, String uid) {
        if (companyId == null) {
            throw new NotAvailableOperationException("Невозможно посадить дерево - у пользователя нет компании");
        }
        Document documentOnSign = documentRepository.findBySignUidAndNotSigned(uid).orElseThrow(() -> new EntityNotFoundException(String.format("Документ с uid %s не найден", uid)));
        documentOnSign.setSigned(true);
        documentOnSign.setSignDate(LocalDateTime.now());
        documentOnSign.setSignUid(null);
        return documentRepository.save(documentOnSign);
    }

    public String getSignUid(Long companyId, Long docId) {
        Document documentOnSign = documentRepository.findByIdAndNotSignedAndCompanyId(companyId, docId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Документ с id %s недоступен для подписания", docId)));

        documentOnSign.setSignUid(UUID.randomUUID().toString());
        return documentRepository.save(documentOnSign).getSignUid();
    }

    public Document createDocument(Long companyId, DocumentDto documentDto) {
        if (companyId == null) {
            throw new NotAvailableOperationException("Невозможно создать документ - у пользователя нет компании");
        }
        Company byId = companyService.findById(companyId);

        Document document = new Document();
        document.setName(documentDto.getName());
        document.setDescription(documentDto.getDescription());
        document.setCompany(byId);
        document.setSignUid(UUID.randomUUID().toString());
        noticeService.sendNotice(documentDto);
        return documentRepository.save(document);
    }

    public static DocumentDto toDto(Document document) {
        return DocumentDto.builder()
                .id(document.getId())
                .createdDate(document.getCreatedDate())
                .signUid(document.getSignUid())
                .name(document.getName())
                .description(document.getDescription())
                .isSigned(document.isSigned())
                .signDate(document.getSignDate())
                .companyId(document.getCompany().getId())
                .build();
    }
}
