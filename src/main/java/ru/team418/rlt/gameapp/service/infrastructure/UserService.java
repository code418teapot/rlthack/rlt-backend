package ru.team418.rlt.gameapp.service.infrastructure;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.team418.rlt.gameapp.dto.UserDto;
import ru.team418.rlt.gameapp.entity.User;
import ru.team418.rlt.gameapp.exceptions.UserExistsException;
import ru.team418.rlt.gameapp.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository repository;
    private final BCryptPasswordEncoder passwordEncoder;

    /**
     * Получаем необходимые данные о пользователе для модуля аутентификации
     *
     * @param username никнейм пользователя
     *
     * @return интерфейс с данными пользователя
     *
     * @throws EntityNotFoundException пользователь не найден
     */
    @Override
    @Cacheable("userDetails")
    public UserDetails loadUserByUsername(String username) throws EntityNotFoundException {
        return findUserByEmail(username);
    }

    /**
     * Получение пользователя по айди
     *
     * @param id айди нужного нам пользователя
     *
     * @return возвращаем найденного пользователя
     */

    public UserDto getUserDtoById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Неверный идентификатор пользователя");
        }
        return repository.findById(id)
                .map(UserService::buildDto)
                .orElse(null);
    }

    /**
     * Редактирование пользователя
     *
     * @param newUserData новые данные пользователя
     *
     * @return возвращает ДТО пользователя
     */
    @CachePut(value = "findUserByUsername", key = "#user.username")
    public UserDto editUserDto(Long userId, UserDto newUserData) {
        User editedUser = this.findUserById(userId);

        editedUser.setFirstName(newUserData.getFirstName());
        editedUser.setLastName(newUserData.getLastName());
        editedUser.setMiddleName(newUserData.getMiddleName());
        editedUser.setEmail(newUserData.getEmail());
        editedUser.setInn(newUserData.getInn());
        editedUser.setPassword(newUserData.getInn());

        return buildDto(repository.save(editedUser));
    }

    /**
     * Получение пользователя по его электронной почте
     *
     * @param email эл. почта пользователя
     *
     * @return данные о пользователе
     */
    @Cacheable(value = "findUserByEmail", key = "#email")
    public User findUserByEmail(String email) {
        return repository
                .findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Пользователь с email = %s не найден", email)));
    }

    /**
     * Получение пользователя по айди
     *
     * @param id айди пользователя
     *
     * @return возвращаем данные о пользователе
     */
    public User findUserById(Long id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Пользователь с id = %s не найден", id)));
    }

    public User findByTelegramId(Long id) {
        return repository.findByTelegramId(id);
    }

    public Set<User> findAllByCompanyId(Long companyId) {
        return repository.findAllByCompanyId(companyId);
    }

    /**
     * Создание пользователя
     *
     * @param dto        данные пользователя для аутентификации
     * @param isAdmin    признак администратора
     * @param telegramId айди телеграм аккаунта (если регистрация произошла из бота)
     */
    @CachePut(value = "findUserByEmail", key = "#dto.email")
    public User createUser(UserDto dto, boolean isAdmin, Long telegramId) {
        if (repository.findByEmail(dto.getEmail()).isEmpty()) {
            User invitedBy = null;
            if (StringUtils.isNotBlank(dto.getInviteUid())) {
                invitedBy = repository.findByInviteUid(dto.getInviteUid())
                        .orElseThrow(() -> new EntityNotFoundException(String.format("Пользователь с реферальным UUID %s не найден", dto.getInviteUid())));
            }
            Set<User.Role> roles = new HashSet<>();
            if (isAdmin) {
                roles.add(User.Role.ADMIN);
            } else {
                roles.add(User.Role.USER);
            }

            var user = new User();
            user.setFirstName(dto.getFirstName());
            user.setLastName(dto.getLastName());
            user.setMiddleName(dto.getMiddleName());
            user.setInn(dto.getInn());
            user.setPassword(passwordEncoder.encode(dto.getPassword()));
            user.setEmail(dto.getEmail());
            user.setActive(true);
            user.setRoles(roles);
            user.setInviteUid(UUID.randomUUID().toString());
            user.setInvitedBy(invitedBy);
            user.setTelegramId(telegramId);

            return repository.save(user);
        } else {
            throw new UserExistsException(String.format("Пользователь с email %s уже существует!", dto.getEmail()));
        }
    }

    /**
     * Удаление пользователя по айди
     *
     * @param id айди пользователя
     */
    public void deleteUser(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Неверный идентификатор пользователя");
        }
        repository.deleteById(id);
    }

    public List<UserDto> getAllUsersWithoutAdmins(String role) {
        return repository.findAllUsersWithoutAdmins(role).stream().map(UserService::buildDto).toList();
    }

    public User save(User user) {
        return repository.save(user);
    }

    public void dropAll() {
        repository.deleteAll();
    }

    /**
     * Превратить сущность в ДТО
     *
     * @param user сущность Пользователь
     *
     * @return ДТО с данными
     */
    private static UserDto buildDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .middleName(user.getMiddleName())
                .email(user.getEmail())
                .inn(user.getInn())
                .password(user.getPassword())
                .active(user.isActive())
                .roles(user.getRoles())
                .inviteUid(user.getInviteUid())
                .invitedBy(user.getInvitedBy() != null ? user.getInvitedBy().getId() : null)
                .telegramId(user.getTelegramId())
                .build();
    }
}
