package ru.team418.rlt.gameapp.service.business;

import org.springframework.stereotype.Service;
import ru.team418.rlt.gameapp.dto.BuyDto;
import ru.team418.rlt.gameapp.dto.DocumentDto;
import ru.team418.rlt.gameapp.entity.User;
import ru.team418.rlt.gameapp.service.infrastructure.UserService;
import ru.team418.rlt.telegram.dto.Notice;
import ru.team418.rlt.telegram.services.NoticeBot;

import java.util.Set;

@Service
public class NoticeService {

    private final NoticeBot bot;
    private final UserService userService;

    public NoticeService(NoticeBot bot, UserService userService) {
        this.bot = bot;
        this.userService = userService;
    }

    public void sendNotice(DocumentDto dto) {
        Set<User> users = userService.findAllByCompanyId(dto.getCompanyId());
        users.forEach(u -> {
            if (u.getTelegramId() != null) {
                bot.sendNotice(new Notice("Подписать", dto.getDescription(), "https://rlthack.team418.ru/edx", "https://images.unsplash.com/photo-1456324504439-367cee3b3c32?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80", u.getTelegramId()));
            }
        });
    }

    public void sendNotice(String description, Long companyId) {
        Set<User> users = userService.findAllByCompanyId(companyId);
        users.forEach(u -> {
            if (u.getTelegramId() != null) {
                bot.sendNotice(description, u.getTelegramId());
            }
        });
    }
}
