package ru.team418.rlt.gameapp.service.infrastructure;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.team418.rlt.gameapp.dto.AchievementDto;
import ru.team418.rlt.gameapp.dto.CompanyDto;
import ru.team418.rlt.gameapp.dto.UserDto;
import ru.team418.rlt.gameapp.entity.*;
import ru.team418.rlt.gameapp.exceptions.UserExistsException;
import ru.team418.rlt.gameapp.service.business.CompanyService;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Сервис, предзаполняющий БД тестовыми данными при первом старте приложения
 */
@Service
@AllArgsConstructor
public class SeedService {

    private final UserService userService;
    private final CompanyService companyService;
    private final RefreshTokenService refreshTokenService;

    @PostConstruct
    public void seed() {
        refreshTokenService.dropAll();
        userService.dropAll();
        companyService.dropAll();

        seedMainUser();
        seedCompany_1();
        joinCompany();
        seedCompany_2();
        seedCompany_25();
        seedCompany_105();
        seedReferalUser();
    }

    private void seedMainUser() {
        String email = "test-user@yandex.ru";

        try {
            userService.findUserByEmail(email);
        } catch (EntityNotFoundException e) {
            UserDto dto = UserDto.builder()
                    .firstName("Тестовый")
                    .lastName("Василий")
                    .middleName("Петрович")
                    .email(email)
                    .inn("123456789012")
                    .password(email)
                    .build();
            userService.createUser(dto, false, null);
        } catch (UserExistsException ignored) {
        }

    }

    private void seedCompany_1() {
        try {
            companyService.findByInn("123456789012");
        } catch (EntityNotFoundException e) {
            ArrayList<Achievement> achievements = new ArrayList<>();
            achievements.add(Achievement.GETTING_STARTED);
            achievements.add(Achievement.FOREST_FRIEND);

            var achDtos = achievements.stream()
                    .map(ach -> new AchievementDto(ach.name(), ach.getBonus()))
                    .toList();


            Company company = companyService.createCompany(new CompanyDto(null, null, null, "ОАО \"МММ\"", "Общество c ограниченной ответственностью \"МММ\"",
                    Region.RU_MOW.toString(), "123456789012", achDtos, 0, 0, null));
            company.getTrees().add(new Tree(50, company));
            company.getTrees().add(new Tree(50, company));
            company.getDocuments().add(new Document("Документ, подтверждающий соответствие участника требованиям тендерной документации", "Описание документа, подтверждающего соответствие участника требованиям тендерной документации", UUID.randomUUID().toString(), false, null, company));
            company.getDocuments().add(new Document("Документ подтверждающий уплату гарантийного взноса", "Описание документа подтверждающего уплату гарантийного взноса", UUID.randomUUID().toString(), false, null, company));
            company.getDocuments().add(new Document("Заявление на участие в торгах на покупку крутых стульев", "Описание заявления на участие в торгах на покупку крутых стульев", null, true, LocalDateTime.now(), company));

            companyService.save(company);
        }

    }

    private void joinCompany() {
        User userByEmail = userService.findUserByEmail("test-user@yandex.ru");
        Company byInn = companyService.findByInn("123456789012");
        companyService.joinCompany(userByEmail.getId(), byInn.getId());
    }

    private void seedReferalUser() {
        String email = "referal-user@yandex.ru";

        try {
            userService.findUserByEmail(email);
        } catch (EntityNotFoundException e) {
            UserDto dto = UserDto.builder()
                    .firstName("Приглашенный")
                    .lastName("Иван")
                    .middleName("Васильевич")
                    .email(email)
                    .inn("210987654321")
                    .password(email)
                    .inviteUid(userService.findUserByEmail("test-user@yandex.ru").getInviteUid())
                    .build();
            User user = userService.createUser(dto, false, 12548632L);
            companyService.createCompany(user, Region.RU_CE.toString());
        } catch (UserExistsException ignored) {
        }

    }

    private void seedCompany_2() {
        try {
            companyService.findByInn("123456789013");
        } catch (EntityNotFoundException e) {
            ArrayList<Achievement> achievements = new ArrayList<>();
            achievements.add(Achievement.GETTING_STARTED);
            achievements.add(Achievement.FOREST_FRIEND);

            var achDtos = achievements.stream()
                    .map(ach -> new AchievementDto(ach.name(), ach.getBonus()))
                    .toList();


            Company company = companyService.createCompany(new CompanyDto(null, null, null, "ОАО \"МММ-2\"", "Общество c ограниченной ответственностью \"МММ-2\"",
                    Region.RU_TYU.toString(), "123456789013", achDtos, 0, 0, null));
            company.getTrees().add(new Tree(50, company));
            company.getTrees().add(new Tree(50, company));

            companyService.save(company);
        }

    }

    private void seedCompany_25() {
        try {
            companyService.findByInn("123456789014");
        } catch (EntityNotFoundException e) {
            ArrayList<Achievement> achievements = new ArrayList<>();
            achievements.add(Achievement.GETTING_STARTED);
            achievements.add(Achievement.FOREST_FRIEND);

            var achDtos = achievements.stream()
                    .map(ach -> new AchievementDto(ach.name(), ach.getBonus()))
                    .toList();


            Company company = companyService.createCompany(new CompanyDto(null, null, null, "ОАО \"МММ-25\"", "Общество c ограниченной ответственностью \"МММ-25\"",
                    Region.RU_KYA.toString(), "123456789014", achDtos, 0, 0, null));
            for (int i = 0; i < 25; i++) {
                company.getTrees().add(new Tree(50, company));
            }
            company.getTrees().add(new Tree(50, company));

            companyService.save(company);
        }

    }

    private void seedCompany_105() {
        try {
            companyService.findByInn("123456789015");
        } catch (EntityNotFoundException e) {
            ArrayList<Achievement> achievements = new ArrayList<>();
            achievements.add(Achievement.GETTING_STARTED);
            achievements.add(Achievement.FOREST_FRIEND);

            var achDtos = achievements.stream()
                    .map(ach -> new AchievementDto(ach.name(), ach.getBonus()))
                    .toList();


            Company company = companyService.createCompany(new CompanyDto(null, null, null, "ОАО \"МММ-105\"", "Общество c ограниченной ответственностью \"МММ-105\"",
                    Region.RU_IRK.toString(), "123456789015", achDtos, 0, 0, null));
            for (int i = 0; i < 105; i++) {
                company.getTrees().add(new Tree(50, company));
            }

            companyService.save(company);
        }

    }
}
