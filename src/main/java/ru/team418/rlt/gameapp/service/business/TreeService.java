package ru.team418.rlt.gameapp.service.business;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.team418.rlt.gameapp.entity.Tree;
import ru.team418.rlt.gameapp.repository.TreeRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class TreeService {

    private TreeRepository treeRepository;

    public List<Tree> findByCompany(Long companyId) {
        return treeRepository.findByCompanyId(companyId);
    }

}
