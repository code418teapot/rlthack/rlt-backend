package ru.team418.rlt.gameapp.service.business;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.team418.rlt.gameapp.dto.AchievementDto;
import ru.team418.rlt.gameapp.dto.BuyDto;
import ru.team418.rlt.gameapp.dto.CompanyDto;
import ru.team418.rlt.gameapp.dto.DocumentDto;
import ru.team418.rlt.gameapp.entity.*;
import ru.team418.rlt.gameapp.exceptions.NotAvailableOperationException;
import ru.team418.rlt.gameapp.exceptions.NotEnoughBonusesException;
import ru.team418.rlt.gameapp.repository.CompanyRepository;
import ru.team418.rlt.gameapp.service.Utils;
import ru.team418.rlt.gameapp.service.infrastructure.UserService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.team418.rlt.gameapp.entity.Achievement.FOREST_FRIEND;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;
    private final UserService userService;
    private final NoticeService noticeService;

    @Value("${bonuses.register}")
    private Integer referralBonus;

    @Value("${cost.plantTree}")
    private Integer plantTreeCost;

    public CompanyService(CompanyRepository companyRepository, UserService userService, NoticeService noticeService) {
        this.companyRepository = companyRepository;
        this.userService = userService;
        this.noticeService = noticeService;
    }

    public Company findById(Long companyId) {
        return companyRepository.findById(companyId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Компания с id %s не найдена", companyId)));
    }

    public void joinCompany(Long userId, Long companyId) {
        User user = userService.findUserById(userId);

        Company byId = findById(companyId);
        user.setCompany(byId);

        userService.save(user);
    }

    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    public Company findByUserId(Long userId) {
        return companyRepository.findByUserId(userId).orElse(null);
    }

    public Company findByInn(String inn) {
        return companyRepository.findByInn(inn)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Компания с ИНН %s не найдена", inn)));
    }

    public void minusBonuses(Long companyId, int count) {
        Company company = findById(companyId);
        if (company.getBonusCount() - count > 0) {
            company.setBonusCount(company.getBonusCount() - count);
            companyRepository.save(company);
        } else {
            throw new NotEnoughBonusesException("Недостаточно средств для списания");
        }
    }

    @Transactional
    public void plantTree(Long companyId) {
        if (companyId == null) {
            throw new NotAvailableOperationException("Невозможно посадить дерево - у пользователя нет компании");
        }
        Company company = findById(companyId);

        Tree tree = new Tree();
        tree.setCompany(company);
        company.getTrees().add(tree);

        minusBonuses(companyId, plantTreeCost);

        if (!company.getAchievements().contains(FOREST_FRIEND)) {
            company.getAchievements().add(FOREST_FRIEND);
        }

        companyRepository.save(company);
    }

    public Company createCompany(CompanyDto companyDto) {
        Region region = Region.fromValue(companyDto.getRegion());
        if (region == null) {
            throw new IllegalArgumentException(String.format("Неверно указан регион компании: %s", companyDto.getRegion()));
        }
        Company company = new Company();
        company.setTitle(companyDto.getTitle());
        company.setFullTitle(companyDto.getFullTitle());
        company.setInn(companyDto.getInn());
        company.setRegion(region);

        return companyRepository.save(company);
    }

    @Transactional
    public Company createCompany(User user, String regionStr) {
        Region region = Region.fromValue(regionStr);
        if (region == null) {
            throw new IllegalArgumentException(String.format("Неверно указан регион компании: %s", regionStr));
        }

        Company company = new Company();
        company.setTitle(Utils.buildInitials(user.getLastName(), user.getFirstName(), user.getMiddleName()));
        company.setFullTitle(Utils.buildFio(user.getLastName(), user.getFirstName(), user.getMiddleName()));
        company.setInn(user.getInn());
        company.setRegion(region);

        if (user.getInvitedBy() != null) {
            Company invitedByCompany = findByUserId(user.getInvitedBy().getId());
            invitedByCompany.setBonusCount(invitedByCompany.getBonusCount() + referralBonus);
            companyRepository.save(invitedByCompany);
            sendAddBonusNotice(referralBonus, invitedByCompany.getBonusCount(), invitedByCompany.getId());

            company.setBonusCount(company.getBonusCount() + referralBonus);
            sendAddBonusNotice(referralBonus, company.getBonusCount(), company.getId());
        }

        Company savedCompany = companyRepository.save(company);
        user.setCompany(savedCompany);
        userService.save(user);
        return savedCompany;
    }

    public Company save(Company user) {
        return companyRepository.save(user);
    }

    public void dropAll() {
        companyRepository.deleteAll();
    }

    public void grabAchievement(Long companyId, String achiementType) {
        Achievement achievement = Achievement.fromValue(achiementType);
        if (achievement == null) {
            throw new IllegalArgumentException(String.format("Неверно указан код достижения: %s", achiementType));
        }

        Company company = findById(companyId);
        if (!company.getAchievements().contains(achievement)) {
            company.getAchievements().add(achievement);
            company.setBonusCount(company.getBonusCount() + achievement.getBonus());
            sendAddBonusNotice(achievement.getBonus(), company.getBonusCount(), companyId);
        }
        companyRepository.save(company);
    }

    private void sendAddBonusNotice(int bonusesDelta, int newBonuses, Long companyId) {
        if (bonusesDelta > 0) {
            StringBuilder sb = new StringBuilder("Вы получили ")
                    .append(bonusesDelta)
                    .append(" бонусов. У вас сейчас ")
                    .append(newBonuses)
                    .append(" бонусов.");
            if (newBonuses >= 50) {
                sb.append(" Вы можете потратить бонусы чтобы посадить дерево, или получить скидку на приобретение услуг.");
            } else if (newBonuses >= 30) {
                sb.append(" Вы можете потратить бонусы чтобы получить скидку на приобретение услуг.");
            }
            noticeService.sendNotice(sb.toString(), companyId);
        }
    }

    public static CompanyDto toDto(Company company) {
        return CompanyDto.builder()
                .id(company.getId())
                .createdDate(company.getCreatedDate())
                .updatedDate(company.getUpdatedDate())
                .title(company.getTitle())
                .fullTitle(company.getFullTitle())
                .region(company.getRegion().toString())
                .inn(company.getInn())
                .achievements(company.getAchievements() != null ? company.getAchievements().stream().map(ach -> new AchievementDto(ach.name(), ach.getBonus())).collect(Collectors.toList()) : null)
                .bonusCount(company.getBonusCount())
                .treesCount(company.getTrees() != null ? company.getTrees().size() : 0)
                .documents(company.getDocuments() != null ? company.getDocuments().stream().map(doc -> new DocumentDto(doc.getId(), doc.getCreatedDate(), doc.getName(), doc.getDescription(), doc.getSignUid(), doc.isSigned(), doc.getSignDate(), doc.getCompany().getId())).toList() : null)
                .build();
    }

    public void buy(Long companyId, BuyDto buyDto) {
        String description;
        if (buyDto.sale() > 0) {
            var company = this.findById(companyId);
            int delta = company.getBonusCount() - saleToBonus(buyDto.sale());
            if (delta >= 0) {
                company.setBonusCount(delta);
                this.save(company);
                description = String.format("Вы потратили %d бонусов при покупе \"%s\"", delta, buyDto.name());
                noticeService.sendNotice(description, companyId);
            } else {
                description = String.format("Недостаточно бонусов для покупки \"%s\". Не хватает %d бонусов", buyDto.name(), delta);
                noticeService.sendNotice(description, companyId);
                throw new NotEnoughBonusesException("Недостаточно Бонусов");
            }
        }
    }

    public int saleToBonus(int sale) {
        return switch (sale) {
            case 0 -> 0;
            case 5 -> 30;
            case 10 -> 70;
            case 15 -> 90;
            default -> throw new IllegalArgumentException("Неподдерживаемая скидка");
        };
    }

    public Map<String, Integer> getPlantedTreesMap() {
        return companyRepository.findAll()
                .stream()
                .collect(Collectors.toMap(company -> company.getRegion().toString(), company -> company.getTrees().size(), Integer::sum));
    }
}
