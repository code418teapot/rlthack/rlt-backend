package ru.team418.rlt.gameapp.exceptions;

/**
 * Исключение "Недоступная операция"
 */
public class NotAvailableOperationException extends RuntimeException {

    public NotAvailableOperationException() {
        super();
    }

    public NotAvailableOperationException(String message) {
        super(message);
    }
}
