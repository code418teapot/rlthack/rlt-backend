package ru.team418.rlt.gameapp.exceptions;

/**
 * Ошибка "Пользователь уже существует"
 */
public class UserExistsException extends RuntimeException {

    public UserExistsException() {
        super();
    }

    public UserExistsException(String message) {
        super(message);
    }
}
