package ru.team418.rlt.gameapp.exceptions;

/**
 * Исключение "Недостаточно средств"
 */
public class NotEnoughBonusesException extends RuntimeException {

    public NotEnoughBonusesException() {
        super();
    }

    public NotEnoughBonusesException(String message) {
        super(message);
    }
}
