package ru.team418.rlt.gameapp.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.team418.rlt.gameapp.dto.UserDto;
import ru.team418.rlt.gameapp.entity.User;
import ru.team418.rlt.gameapp.service.business.CompanyService;
import ru.team418.rlt.gameapp.service.infrastructure.UserService;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/public/users")
@Tag(name = "Пользователи", description = "API управления пользователями системы")
public class PublicUserController {

    private final UserService userService;

    private final CompanyService companyService;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Регистрация пользователя")
    public void register(@RequestParam(name = "telegramId", required = false) @Parameter(description = "Айди пользователя в телеграме") Long telegramId,
                         @Parameter(description = "Данные пользователя") @Valid @RequestBody UserDto registerData) {
        User user = userService.createUser(registerData, false, telegramId);
        companyService.createCompany(user, registerData.getRegion()); // по умолчанию пользователь = компания
    }

    /*@PostMapping("/register/admin")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Регистрация администратора")
    public void registerAdmin(@Parameter(description = "Данные администратора") @Valid @RequestBody UserDto registerData) {
        userService.createUser(registerData, true, null);
    }*/

}
