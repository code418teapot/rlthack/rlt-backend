package ru.team418.rlt.gameapp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.team418.rlt.gameapp.dto.BuyDto;
import ru.team418.rlt.gameapp.dto.CompanyDto;
import ru.team418.rlt.gameapp.entity.Company;
import ru.team418.rlt.gameapp.service.business.CompanyService;

import javax.validation.Valid;
import java.util.List;

import static ru.team418.rlt.gameapp.advices.RequestFilter.COMPANY_ID;
import static ru.team418.rlt.gameapp.advices.RequestFilter.USER_ID;
import static ru.team418.rlt.gameapp.service.business.CompanyService.toDto;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/companies")
@Tag(name = "Компании", description = "API управления компаниями")
public class CompanyController {

    private final CompanyService companyService;

    @PostMapping("/create")
    @Operation(description = "Регистрация компании")
    public CompanyDto createCompany(@Parameter(description = "Данные пользователя") @Valid @RequestBody CompanyDto registerData) {
        return toDto(companyService.createCompany(registerData));
    }

    @PostMapping("/join/{companyId}")
    @Operation(description = "Присоединиться к компании")
    public void createCompany(@RequestAttribute(USER_ID) Long userId, @PathVariable Long companyId) {
        companyService.joinCompany(userId, companyId);
    }

    @GetMapping
    @Operation(description = "Получить информацию о всем компаниях")
    public List<CompanyDto> getAllCompanies() {
        return companyService.findAll().stream().map(CompanyService::toDto).toList();
    }

    @GetMapping("/my-сompany")
    @Operation(description = "Получить информацию о компании текущего пользователя")
    public CompanyDto getMyCompanyInfo(@RequestAttribute(USER_ID) Long userId) {
        Company company = companyService.findByUserId(userId);
        if (company == null) {
            return null;
        } else {
            return toDto(company);
        }
    }

    @PostMapping("/trees/plant")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Посадить дерево для компании текущего пользователя")
    public void plantTree(@RequestAttribute(COMPANY_ID) Long companyId) {
        companyService.plantTree(companyId);
    }

    @PutMapping("/buySomething")
    @Operation(description = "Купить")
    public void buySomething(@RequestAttribute(COMPANY_ID) Long companyId, @RequestBody BuyDto buyDto) {
        companyService.buy(companyId, buyDto);
    }

    @PutMapping("/grab-achievement")
    @Operation(description = "Выдать достижение компании")
    public void grabAchievement(@RequestAttribute(COMPANY_ID) Long companyId,
                                @RequestParam(name = "achievementType") @Parameter(description = "Код достижения") String achievementType) {
        companyService.grabAchievement(companyId, achievementType);
    }
}
