package ru.team418.rlt.gameapp.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.team418.rlt.gameapp.dto.UserDto;
import ru.team418.rlt.gameapp.service.infrastructure.UserService;
import ru.team418.rlt.telegram.services.TelegramIntegrationService;

import static ru.team418.rlt.gameapp.advices.RequestFilter.USER_ID;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/users")
@Tag(name = "Пользователи", description = "API управления пользователями системы")
public class UserController {

    private final UserService userService;
    private final TelegramIntegrationService telegramIntegrationService;

    @GetMapping("/profile")
    @Operation(description = "Получение инфо о текущем пользователе")
    public UserDto getCurrentProfileInfo(@RequestAttribute(USER_ID) Long userId) {
        return userService.getUserDtoById(userId);
    }

    @GetMapping("/{id}")
    @Operation(description = "Получение инфо о пользователе")
    public UserDto getProfileInfo(@Parameter(description = "Идентификатор пользователя") @PathVariable Long id) {
        return userService.getUserDtoById(id);
    }

    @GetMapping("/integration/telegram")
    @Operation(description = "Получение токина для интеграции с телеграммом")
    public String telegramIntegration(@RequestAttribute(USER_ID) Long userId) {
        return telegramIntegrationService.generateCode(userId);
    }
}