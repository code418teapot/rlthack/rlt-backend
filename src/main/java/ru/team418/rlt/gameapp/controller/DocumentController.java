package ru.team418.rlt.gameapp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.team418.rlt.gameapp.dto.DocumentDto;
import ru.team418.rlt.gameapp.service.business.DocumentService;

import javax.validation.Valid;

import static ru.team418.rlt.gameapp.advices.RequestFilter.COMPANY_ID;
import static ru.team418.rlt.gameapp.service.business.DocumentService.toDto;

@RestController
@AllArgsConstructor
@RequestMapping("/protected/documents")
@Tag(name = "Документы", description = "API управления документами")
public class DocumentController {

    private final DocumentService documentService;

    @PostMapping("/create")
    @Operation(description = "Создать документ")
    public DocumentDto createDocument(@RequestAttribute(COMPANY_ID) Long companyId, @Parameter(description = "Данные документа") @Valid @RequestBody DocumentDto documentDto) {
        return toDto(documentService.createDocument(companyId, documentDto));
    }

    @PostMapping("/sign")
    @Operation(description = "Подписать документ")
    public DocumentDto getMyCompanyInfo(@RequestAttribute(COMPANY_ID) Long companyId, @RequestParam(name = "uid") String uid) {
        return toDto(documentService.sign(companyId, uid));
    }

    @GetMapping("/{docId}/get-sign-uid")
    @Operation(description = "Получить UID на подписание документа")
    public String getSignUid(@RequestAttribute(COMPANY_ID) Long companyId, @PathVariable(name = "docId") Long docId) {
        return documentService.getSignUid(companyId, docId);
    }


}
