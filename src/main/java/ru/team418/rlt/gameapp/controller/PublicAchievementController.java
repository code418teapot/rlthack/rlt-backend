package ru.team418.rlt.gameapp.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.team418.rlt.gameapp.dto.AchievementDto;
import ru.team418.rlt.gameapp.entity.Achievement;

import java.util.Arrays;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/public/achievements")
@Tag(name = "Достижения", description = "API управления достижениями пользователей системы")
public class PublicAchievementController {

    @GetMapping
    @Operation(description = "Получение инфо об ачивках")
    public List<AchievementDto> getAchievementsList() {
        return Arrays
                .stream(Achievement.values())
                .map(ach -> new AchievementDto(ach.name(), ach.getBonus()))
                .toList();
    }

}
