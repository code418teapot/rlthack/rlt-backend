package ru.team418.rlt.gameapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.team418.rlt.gameapp.entity.RefreshToken;

import java.util.Optional;
import java.util.UUID;

/**
 * Интерфейс для работы с таблицей refresh-токенов в БД
 */
@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    Optional<RefreshToken> findByToken(UUID token);
}
