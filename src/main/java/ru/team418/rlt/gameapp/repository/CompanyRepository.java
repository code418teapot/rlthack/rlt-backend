package ru.team418.rlt.gameapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.team418.rlt.gameapp.entity.Company;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    Optional<Company> findByTitle(String title);

    Optional<Company> findByInn(String inn);

    @Query(value = "select * from public.company c join users u on u.company_id = c.id where u.id = :userId", nativeQuery = true)
    Optional<Company> findByUserId(@Param("userId") Long userId);
}
