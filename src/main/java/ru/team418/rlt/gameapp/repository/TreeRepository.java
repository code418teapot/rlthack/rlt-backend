package ru.team418.rlt.gameapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.team418.rlt.gameapp.entity.Tree;

import java.util.List;

public interface TreeRepository extends JpaRepository<Tree, Long> {

    @Query(value = "select * from public.tree t join company c on t.company_id = c.id where c.id = :companyId", nativeQuery = true)
    List<Tree> findByCompanyId(@Param("companyId") Long companyId);
}
