package ru.team418.rlt.gameapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.team418.rlt.gameapp.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Интерфейс для работы с таблицей пользователей в БД
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Override
    Optional<User> findById(Long id);

    Optional<User> findByEmail(String email);
    Optional<User> findByInviteUid(String inviteUid);

    @Modifying
    @Query("update User u set u.inviteUid = :inviteUid where u.id = :id")
    void setInviteUid(@Param("id") Long id, @Param("inviteUid") String inviteUid);

    @Query(value = "select * from public.users u join user_roles r on r.user_id = u.id where not r.role = :roleName", nativeQuery = true)
    List<User> findAllUsersWithoutAdmins(@Param("roleName") String roleName);

    Set<User> findAllByCompanyId(Long companyId);

    User findByTelegramId(Long id);
}
