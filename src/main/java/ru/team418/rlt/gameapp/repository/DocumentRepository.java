package ru.team418.rlt.gameapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.team418.rlt.gameapp.entity.Document;

import java.util.List;
import java.util.Optional;

public interface DocumentRepository extends JpaRepository<Document, Long> {

    @Query(value = "select * from public.document d join company c on d.company_id = c.id where c.id = :companyId", nativeQuery = true)
    List<Document> findByCompanyId(@Param("companyId") Long companyId);

    @Query(value = "select * from public.document d join company c on d.company_id = c.id where d.sign_uid = :signUid and d.is_signed is false", nativeQuery = true)
    Optional<Document> findBySignUidAndNotSigned(@Param("signUid") String signUid);

    @Query(value = "select * from public.document d join company c on d.company_id = c.id where d.id = :docId and d.is_signed is false and d.company_id = :companyId", nativeQuery = true)
    Optional<Document> findByIdAndNotSignedAndCompanyId(@Param("companyId")Long companyId, @Param("docId")Long docId);
}
