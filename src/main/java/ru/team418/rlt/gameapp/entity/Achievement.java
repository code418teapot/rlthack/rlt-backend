package ru.team418.rlt.gameapp.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum Achievement {

    IN_STREAM("IN_STREAM", "В потоке", 10),
    FULL_PACKET("FULL_PACKET", "Полный пакет", 20),
    GETTING_STARTED("GETTING_STARTED", "Начало положено", 20),
    FIRST_SUCCESS("FIRST_SUCCESS", "Первый успех", 40),
    EXPERIENCED_PLAYER("EXPERIENCED_PLAYER", "Опытный игрок", 80),
    FOREST_FRIEND("FOREST_FRIEND", "Друг леса", 0),
    BUY_EDO("BUY_EDO", "Никакой бумаги", 60),
    SIGN_DOC_EDO("SIGN_DOC_EDO", "Дело в цифре", 30);

    private final String code;
    private final String name;
    private final int bonus;

    Achievement(String code, String name, int bonus) {
        this.code = code;
        this.name = name;
        this.bonus = bonus;
    }

    public int getBonus() {
        return bonus;
    }

    public String getName() {
        return name;
    }

    @Override
    @JsonValue
    public String toString() {
        return code;
    }

    /**
     * Получить значение енума из строки
     *
     * @param text строковое представление значения енума (напр. "RUB")
     *
     * @return значение енума (Currency.RUB)
     */
    @JsonCreator
    public static Achievement fromValue(String text) {
        return Arrays.stream(Achievement.values())
                .filter(candidate -> candidate.code.equals(text))
                .findFirst()
                .orElse(null);
    }
}
