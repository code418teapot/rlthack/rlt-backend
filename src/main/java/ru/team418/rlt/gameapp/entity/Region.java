package ru.team418.rlt.gameapp.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum Region {

    RU_MOW("RU-MOW", "Москва"),
    RU_SPE("RU-SPE", "Санкт-Петербург"),
    RU_NEN("RU-NEN", "Ненецкий АО"),
    RU_YAR("RU-YAR", "Ярославская область"),
    RU_CHE("RU-CHE", "Челябинская область"),
    RU_ULY("RU-ULY", "Ульяновская область"),
    RU_TYU("RU-TYU", "Тюменская область"),
    RU_TUL("RU-TUL", "Тульская область"),
    RU_SVE("RU-SVE", "Свердловская область"),
    RU_RYA("RU-RYA", "Рязанская область"),
    RU_ORL("RU-ORL", "Орловская область"),
    RU_OMS("RU-OMS", "Омская область"),
    RU_NGR("RU-NGR", "Новгородская область"),
    RU_LIP("RU-LIP", "Липецкая область"),
    RU_KRS("RU-KRS", "Курская область"),
    RU_KGN("RU-KGN", "Курганская область"),
    RU_KGD("RU-KGD", "Калининградская область"),
    RU_IVA("RU-IVA", "Ивановская область"),
    RU_BRY("RU-BRY", "Брянская область"),
    RU_AST("RU-AST", "Астраханская область"),
    RU_KHA("RU-KHA", "Хабаровский край"),
    RU_CE("RU-CE", "Чеченская республика"),
    RU_UD("RU-UD", "Удмуртская республика"),
    RU_SE("RU-SE", "Республика Северная Осетия"),
    RU_MO("RU-MO", "Республика Мордовия"),
    RU_KR("RU-KR", "Республика Карелия"),
    RU_KL("RU-KL", "Республика Калмыкия"),
    RU_IN("RU-IN", "Республика Ингушетия"),
    RU_AL("RU-AL", "Республика Алтай"),
    RU_BA("RU-BA", "Республика Башкортостан"),
    RU_AD("RU-AD", "Республика Адыгея"),
    RU_CR("RU-CR", "Республика Крым"),
    RU_SEV("RU-SEV", "Севастополь"),
    RU_KO("RU-KO", "Республика Коми"),
    RU_KIR("RU-KIR", "Кировская область"),
    RU_PNZ("RU-PNZ", "Пензенская область"),
    RU_TAM("RU-TAM", "Тамбовская область"),
    RU_MUR("RU-MUR", "Мурманская область"),
    RU_LEN("RU-LEN", "Ленинградская область"),
    RU_VLG("RU-VLG", "Вологодская область"),
    RU_KOS("RU-KOS", "Костромская область"),
    RU_PSK("RU-PSK", "Псковская область"),
    RU_ARK("RU-ARK", "Архангельская область"),
    RU_YAN("RU-YAN", "Ямало-Ненецкий АО"),
    RU_CHU("RU-CHU", "Чукотский АО"),
    RU_YEV("RU-YEV", "Еврейская автономная область"),
    RU_TY("RU-TY", "Республика Тыва"),
    RU_SAK("RU-SAK", "Сахалинская область"),
    RU_AMU("RU-AMU", "Амурская область"),
    RU_BU("RU-BU", "Республика Бурятия"),
    RU_KK("RU-KK", "Республика Хакасия"),
    RU_KEM("RU-KEM", "Кемеровская область"),
    RU_NVS("RU-NVS", "Новосибирская область"),
    RU_ALT("RU-ALT", "Алтайский край"),
    RU_DA("RU-DA", "Республика Дагестан"),
    RU_STA("RU-STA", "Ставропольская область"),
    RU_KB("RU-KB", "Кабардино-Балкарская республика"),
    RU_KC("RU-KC", "Карачаевая-Черкесская республика"),
    RU_KDA("RU-KDA", "Краснодарский край"),
    RU_ROS("RU-ROS", "Ростовская область"),
    RU_SAM("RU-SAM", "Самарская область"),
    RU_TA("RU-TA", "Республика Татарстан"),
    RU_ME("RU-ME", "Республика Марий Эл"),
    RU_CU("RU-CU", "Чувашская республика"),
    RU_NIZ("RU-NIZ", "Нижегородская область"),
    RU_VLA("RU-VLA", "Владимировская область"),
    RU_MOS("RU-MOS", "Московская область"),
    RU_KLU("RU-KLU", "Калужская область"),
    RU_BEL("RU-BEL", "Белгородская область"),
    RU_ZAB("RU-ZAB", "Забайкальский край"),
    RU_PRI("RU-PRI", "Приморский край"),
    RU_KAM("RU-KAM", "Камчатский край"),
    RU_MAG("RU-MAG", "Магаданская область"),
    RU_SA("RU-SA", "Республика Саха (Якутия)"),
    RU_KYA("RU-KYA", "Красноярский край"),
    RU_ORE("RU-ORE", "Оренбургская область"),
    RU_SAR("RU-SAR", "Саратовская область"),
    RU_VGG("RU-VGG", "Волгоградская область"),
    RU_VOR("RU-VOR", "Ставропольский край"),
    RU_SMO("RU-SMO", "Смоленская область"),
    RU_TVE("RU-TVE", "Тверская область"),
    RU_PER("RU-PER", "Пермская область"),
    RU_KHM("RU-KHM", "Ханты-Мансийский АО"),
    RU_TOM("RU-TOM", "Томская область"),
    RU_IRK("RU-IRK", "Иркутская область");

    private final String id;
    private final String name;

    Region(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    @JsonValue
    public String toString() {
        return id;
    }

    /**
     * Получить значение енума из строки
     *
     * @param text строковое представление значения енума (напр. "RU-MOW")
     *
     * @return значение енума (Currency.RU_MOW)
     */
    @JsonCreator
    public static Region fromValue(String text) {
        return Arrays.stream(Region.values())
                .filter(candidate -> candidate.id.equals(text))
                .findFirst()
                .orElse(null);
    }
}
