package ru.team418.rlt.gameapp.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "document")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Document extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "sign_uid")
    private String signUid = UUID.randomUUID().toString();

    @Column(name = "is_signed")
    private boolean isSigned = false;

    @Column(name = "sign_date")
    private LocalDateTime signDate;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(columnDefinition = "integer", name = "company_id")
    private Company company;

}
