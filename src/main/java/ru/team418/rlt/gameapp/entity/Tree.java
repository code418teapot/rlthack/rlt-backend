package ru.team418.rlt.gameapp.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tree")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tree extends BaseEntity {

    @Transient
    private int cost = 50;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(columnDefinition = "integer", name = "company_id")
    private Company company;

}
