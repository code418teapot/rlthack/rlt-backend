package ru.team418.rlt.gameapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Данные о документе")
public class DocumentDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 599053845034850L;

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Дата создания")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime createdDate;

    @Schema(description = "Название")
    private String name;

    @Schema(description = "Описание")
    private String description;

    @Schema(description = "Одноразовый UID для подписания документа")
    private String signUid;

    @Schema(description = "Признак документ подписан")
    private boolean isSigned;

    @Schema(description = "Дата и время подписания")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime signDate;

    @Schema(description = "Айди компании")
    private Long companyId;
}