package ru.team418.rlt.gameapp.dto;

public record BuyDto(int sum, int sale, String name) {
}
