package ru.team418.rlt.gameapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Данные по достижению")
public class AchievementDto {

    @Schema(description = "Тип (английский)")
    private String type;

    @Schema(description = "Кол-во бонусов за ачивку")
    private int bonus;
}
