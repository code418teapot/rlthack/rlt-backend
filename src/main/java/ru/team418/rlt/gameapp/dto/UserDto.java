package ru.team418.rlt.gameapp.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import ru.team418.rlt.gameapp.entity.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Данные пользователя")
public class UserDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 599053845034850L;

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Имя")
    @NotBlank
    private String firstName;

    @Schema(description = "Фамилия")
    @NotBlank
    private String lastName;

    @Schema(description = "Отчество")
    private String middleName;

    @Schema(description = "Регион пользователя")
    private String region;

    @Email
    @NotBlank
    @Schema(description = "Электронная почта")
    private String email;

    @Schema(description = "ИНН")
    @NotBlank
    private String inn;

    @NotNull
    @Schema(description = "Пароль")
    private String password;

    @Schema(description = "Признак активности/бана пользователя")
    private boolean active;

    @Schema(description = "Список ролей")
    private Set<User.Role> roles;

    @Schema(description = "Реферальная ссылка-uid")
    private String inviteUid;

    @Schema(description = "Идентификатор пользователя, который пригласил текущего пользователя")
    private Long invitedBy;

    @Schema(description = "Идентификатор профиля в телеграмме")
    private Long telegramId;
}
