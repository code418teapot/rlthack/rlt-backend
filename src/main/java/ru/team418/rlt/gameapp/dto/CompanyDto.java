package ru.team418.rlt.gameapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Schema(description = "Данные компании")
public class CompanyDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 599053842034851L;

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Дата создания")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime createdDate;

    @Schema(description = "Дата редактирования")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime updatedDate;

    @Schema(description = "Наименование компании")
    private String title;

    @Schema(description = "Полное наименование компании")
    private String fullTitle;

    @Schema(description = "Регион компании")
    private String region;

    @Schema(description = "ИНН")
    private String inn;

    @Schema(description = "Список достижений")
    private List<AchievementDto> achievements;

    @Schema(description = "Количество бонусов")
    private int bonusCount;

    @Schema(description = "Количество посаженных деревьев")
    private int treesCount;

    @Schema(description = "Список документов")
    private List<DocumentDto> documents;
}